# *-* coding=utf8 *-*

from .mbr import mbr_part_types
from .uefi import uefi_part_types

__all__ = ['mbr_part_types', 'uefi_part_types']
