SHELL=/bin/bash
PKG_NAME=pyuefi

default: clean register upload
	@echo "Full service complete"

clean:
	@echo "Removing the build/ dist/ and *.egg-info/ directories"
	@rm -rf build dist *.egg-info

register: version
	@echo "Bundling the code"; echo
	@python3 setup.py sdist bdist_wheel
	@echo; echo "Registering this version of the package on PyPI"; echo
	@for bundle in dist/*; do twine register $$bundle; done

version:
	@echo "Making sure the version numbers and required packages are consistent"
	@sed -r "s/\{\{version\}\}/`python3 -c 'import $(PKG_NAME); print($(PKG_NAME).__version__)'`/" .setup.base > setup.py
	@sed -r -i "s/\{\{reqs\}\}/`python3 -c \"print(', '.join(['\'{}\''.format(x.strip()) for x in open('requirements.txt')]))\"`/" setup.py

upload:
	@echo "Uploading built package to PyPI"
	@twine upload dist/*
