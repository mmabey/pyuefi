======
PyUEFI
======

PyUEFI is a pure Python tool for extracting UEFI partition information and layout from hard drives.

Credit for the information on partition types:

* MBR partitions: Andries Brouwer `<http://www.win.tue.nl/~aeb/partitions/partition_types-1.html>`_
* UEFI partitions: `<http://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs>`_

In the interest of full disclosure, I'm writing this as part of another project. Once the code
fulfills the needs of that other project, I may not make pyuefi generic.

PyUEFI should work under Python versions 2.7 and 3.3+. Please open a ticket if you find this isn't the case.


License
-------

This project is licensed under the `MIT license <http://opensource.org/licenses/mit-license.php>`_.
